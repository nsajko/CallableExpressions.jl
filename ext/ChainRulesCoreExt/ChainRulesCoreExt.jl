module ChainRulesCoreExt

# TODO: thunk

using ChainRulesCore: ChainRulesCore
using CallableExpressions

const Alia = ExpressionTypeAliases
const evaluated = CallableExpressions.ExpressionEvaluated.expression_evaluated

const Arguments0 = Union{NamedTuple{(),Tuple{}},Tuple{},Nothing}
const ArgumentsNonempty = NamedTuple{Names,T} where {T<:Tuple,Names}
const Arguments1 = ArgumentsNonempty{<:Tuple{Any}}

const unthunk = ChainRulesCore.unthunk
const RuleConf = ChainRulesCore.RuleConfig
const RuleConfForward = RuleConf{>:ChainRulesCore.HasForwardsMode}
const RuleConfReverse = RuleConf{>:ChainRulesCore.HasReverseMode}
const no_tangent = ChainRulesCore.NoTangent()
const zero_tangent = ChainRulesCore.ZeroTangent()
const Tangent = ChainRulesCore.Tangent{A} where {A<:ArgumentsNonempty}
const MaybeTangent = Union{Tangent,typeof(no_tangent)}
const symbolic_autodiff_not_implemented_tangent = ChainRulesCore.@not_implemented ""

function make_tangent(::Type{A}, t::Tuple) where {A<:ArgumentsNonempty}
    Tangent{A}(; A(t)...)
end

function pullback_result(ȳ::MaybeTangent)
    (no_tangent, symbolic_autodiff_not_implemented_tangent, ȳ)
end

function zero_or_identity(x, is_identity::Bool)
    if is_identity
        x
    else
        x - x  # TODO: `zero_tangent`?
    end
end

struct PullbackForConstantOrIdentity{A<:Arguments1}
    is_for_identity::Bool  # TODO: move into type domain?
end

function (pb::PullbackForConstantOrIdentity{A})(ȳ) where {A}
    s = zero_or_identity(ȳ, pb.is_for_identity)
    t = make_tangent(A, (unthunk(s),))
    pullback_result(t)
end

function f_rule_for_not_differentiable(e::Alia.ExpressionLoosely, x::Arguments0)
    y = evaluated(e, x)
    (y, no_tangent)
end

function r_rule_for_not_differentiable(e::Alia.ExpressionLoosely, x::Arguments0)
    y = evaluated(e, x)
    pullback = Returns(pullback_result(no_tangent))
    (y, pullback)
end

function f_rule_expression_evaluated_recursive(config::RuleConfForward, e::Alia.ExpressionLoosely, x::A, Δx) where {A<:Arguments1}
    if e isa Union{Alia.Constant,Alia.Variable}
        let y = evaluated(e, x),
            t = zero_or_identity(Δx, e isa Alia.Variable)
            (y, t)
        end
    else
        e::Alia.Expression
        let operation = expression_operation(e),
            children = expression_children(e),
            f = let config = config, x = x, Δx = Δx, recursive_mapping
                function recursive_mapping(child)
                    f_rule_expression_evaluated_recursive(config, child, x, Δx)
                end
            end,
            children_evaluated = map(f, children),
            children_ys = map(first, children_evaluated),
            children_tangents = map(last, children_evaluated),
            (y, operation_tangent) = ChainRulesCore.frule_via_ad(config, (no_tangent, children_tangents...), operation, children_ys...)
            (y, operation_tangent)
        end
    end::NTuple{2,Any}
end

function r_rule_expression_evaluated_recursive(config::RuleConfReverse, e::Alia.ExpressionLoosely, x::A) where {A<:Arguments1}
    if e isa Union{Alia.Constant,Alia.Variable}
        let y = evaluated(e, x),
            pullback = PullbackForConstantOrIdentity{A}(e isa Alia.Variable)
            (y, pullback)
        end
    else
        e::Alia.Expression
        let operation = expression_operation(e),
            children = expression_children(e),
            f = let config = config, x = x, recursive_mapping
                function recursive_mapping(child)
                    r_rule_expression_evaluated_recursive(config, child, x)
                end
            end,
            children_evaluated = map(f, children),
            children_ys = map(first, children_evaluated),
            children_pullbacks = map(last, children_evaluated),
            (y, operation_pullback) = ChainRulesCore.rrule_via_ad(config, operation, children_ys...),
            pullback = let children_pullbacks = children_pullbacks, operation_pullback = operation_pullback, expression_pullback_closure
                function expression_pullback_closure(ȳ)
                    (_, p...) = operation_pullback(ȳ)
                    function g(pullback, tangent)
                        pullback(tangent)
                    end
                    r = map(g, children_pullbacks, p)
                    if isempty(r)
                        pullback_result(make_tangent(A, (zero(only(x)),)))  # TODO: `zero_tangent`?
                    else
                        let tangent_sum
                            function tangent_sum(l::NTuple{3,Any}, r::NTuple{3,Any})
                                (_, _, a) = l
                                (_, _, b) = r
                                pullback_result(a + b)
                            end
                            reduce(tangent_sum, r)
                        end
                    end
                end
            end
            (y, pullback)
        end
    end::NTuple{2,Any}
end

# TODO: XXX: assuming the second of three tangents is zero
function ChainRulesCore.frule(::RuleConf, ::Tuple{Any,Any,Any}, ::typeof(evaluated), e::Alia.ExpressionLoosely, x::Arguments0)
    f_rule_for_not_differentiable(e, x)
end

# TODO: XXX: assuming the second of three tangents is zero
function ChainRulesCore.frule(config::RuleConfForward, (_, _, Δx)::Tuple{Any,Any,Any}, ::typeof(evaluated), e::Alia.ExpressionLoosely, x::Arguments1)
    f_rule_expression_evaluated_recursive(config, e, x, Δx)
end

function ChainRulesCore.rrule(::RuleConf, ::typeof(evaluated), e::Alia.ExpressionLoosely, x::Arguments0)
    r_rule_for_not_differentiable(e, x)
end

function ChainRulesCore.rrule(config::RuleConfReverse, ::typeof(evaluated), e::Alia.ExpressionLoosely, x::Arguments1)
    r_rule_expression_evaluated_recursive(config, e, x)
end

end
