module TermInterfaceExt

using TermInterface: TermInterface
using CallableExpressions

const Alia = ExpressionTypeAliases

function TermInterface.isexpr(::Alia.Expression)
    true
end

function TermInterface.iscall(::Alia.Expression)
    true
end

function TermInterface.arguments(e::Alia.Expression)
    CallableExpressions.expression_children(e)
end

function TermInterface.children(e::Alia.Expression)
    CallableExpressions.expression_children(e)
end

function TermInterface.head(e::Alia.Expression)
    CallableExpressions.expression_operation(e)
end

function TermInterface.operation(e::Alia.Expression)
    CallableExpressions.expression_operation(e)
end

function TermInterface.maketerm(::Type{T}, operation, children, ::Any = nothing, metadata = nothing) where {T<:Alia.Expression}
    CallableExpressions.expression_new(T, operation, children, metadata)
end

end
