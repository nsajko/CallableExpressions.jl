module AbstractTreesExt

using AbstractTrees: AbstractTrees
using CallableExpressions

const Alia = ExpressionTypeAliases

function AbstractTrees.children(e::Alia.Expression)
    CallableExpressions.expression_children(e)
end

function AbstractTrees.nodevalue(e::Alia.Expression)
    CallableExpressions.expression_operation(e)
end

end
