using CallableExpressions
using Test
using Aqua

function differentiation_interface_univariate_function(x)
    expression = StaticExpression(
        (
            StaticExpression((DynamicVariable(:x),), cos),
            StaticExpression(
                (
                    StaticExpression((DynamicVariable(:x),), sin),
                    DynamicConstant(0.75),
                ),
                *,
            ),
        ),
        hypot,
    )
    expression((; x = x))
end

function differentiation_interface_evaluate(operator, backend, x)
    func = differentiation_interface_univariate_function
    if (backend == DifferentiationInterface.AutoZygote()) & (v"1.11.0-" < VERSION)
        @inferred operator(func, backend, x)
    else
        operator(func, backend, x)
    end
end

function differentiation_interface_backend_mapping_maker(operator, x)
    let operator = operator, x = x
        function differentiation_interface_backend_mapping(backend)
            differentiation_interface_evaluate(operator, backend, x)
        end
    end
end

@testset "CallableExpressions.jl" begin
    @testset "Code quality (Aqua.jl)" begin
        Aqua.test_all(CallableExpressions)
    end

    @testset "ExpressionTypeAliases" begin
        A = ExpressionTypeAliases
        @test A.Constant isa Type
        @test A.Variable isa Type
        @test A.Expression isa Type
        @test A.ExpressionLoosely isa Type
    end

    @testset "simple construction and evaluation" begin
        expressions = Any[
            DynamicConstant(3), StaticConstant{3}(),
            DynamicVariable(:x), StaticVariable{:x}(),
            DynamicExpression((), :op, (; op = Returns(3))),
            StaticExpression((), Returns(3)),
            MoreStaticExpression{<:Any,Returns(3)}(()),
        ]
        for _ ∈ 1:3
            for expr ∈ copy(expressions)
                e = DynamicExpression((expr,), :op, (; op = identity))
                push!(expressions, e)
            end
            for expr ∈ copy(expressions)
                e = StaticExpression((expr,), identity)
                push!(expressions, e)
            end
            for expr ∈ copy(expressions)
                e = MoreStaticExpression{<:Any,identity}((expr,))
                push!(expressions, e)
            end
        end
        for expr ∈ expressions
            @test expr((; x = 3)) === 3
        end
    end

    @testset "constant value" begin
        for c ∈ (DynamicConstant(3), StaticConstant{3}())
            @test CallableExpressions.constant_value(c) === 3
        end
    end

    @testset "variable name" begin
        for v ∈ (DynamicVariable(:x), StaticVariable{:x}())
            @test CallableExpressions.variable_name(v) === :x
        end
    end

    @testset "expression operation, expression children" begin
        @test isone(length(methods(expression_children)))
        for e ∈ (
            DynamicExpression((), :op, (; op = Returns(3))),
            StaticExpression((), Returns(3)),
            MoreStaticExpression{<:Any,Returns(3)}(()),
        )
            @test expression_operation(e) === Returns(3)
            @test expression_children(e) === ()
        end
    end

    @testset "equality and hashing" begin
        @testset "elementwise helpers" begin
            # ```julia
            # using Combinatorics
            # f(x) = sort(collect(Set(collect(x))))
            # g(n) = f(permutations(Any[3, 3, 7, 7, missing, missing], n))
            # r = [g(0), g(1), g(2), g(3)];
            # println(r)
            # ```
            collections = Vector{Vector{Any}}[
                [[]], [[3], [7], [missing]],
                [
                    [3, 3], [3, 7], [3, missing], [7, 3], [7, 7], [7, missing], [missing, 3],
                    [missing, 7], [missing, missing],
                ],
                [
                    [3, 3, 7], [3, 3, missing], [3, 7, 3], [3, 7, 7], [3, 7, missing],
                    [3, missing, 3], [3, missing, 7], [3, missing, missing], [7, 3, 3],
                    [7, 3, 7], [7, 3, missing], [7, 7, 3], [7, 7, missing], [7, missing, 3],
                    [7, missing, 7], [7, missing, missing], [missing, 3, 3], [missing, 3, 7],
                    [missing, 3, missing], [missing, 7, 3], [missing, 7, 7],
                    [missing, 7, missing], [missing, missing, 3], [missing, missing, 7],
                ],
            ]
            EH = CallableExpressions.EqualityAndHashing
            for c ∈ collections
                for a ∈ c
                    for b ∈ c
                        @test EH.elementwise_equality(a, b) === ==(a, b)
                        @test EH.elementwise_isequal(a, b) === isequal(a, b)
                    end
                end
            end
        end
        @testset "not equal" begin
            ca = (
                DynamicConstant(3), StaticConstant{3}(),
                DynamicVariable(:x), StaticVariable{:x}(),
                DynamicExpression((), :op, (; op = Returns(3))),
                StaticExpression((), Returns(3)),
                MoreStaticExpression{<:Any,Returns(3)}(()),
            )
            cb = (
                DynamicConstant(7), StaticConstant{7}(),
                DynamicVariable(:y), StaticVariable{:y}(),
                DynamicExpression((), :op, (; op = Returns(7))),
                StaticExpression((), Returns(7)),
                MoreStaticExpression{<:Any,Returns(7)}(()),
            )
            for a ∈ ca
                for b ∈ cb
                    @test a != b
                    @test !isequal(a, b)
                    for h ∈ (0x562d1f6bff13f8bff17c48aae07294ea,)
                        g = h % UInt
                        @test hash(a, g)::UInt != hash(b, g)::UInt
                    end
                end
            end
        end
        @testset "equal" begin
            classes = (
                (DynamicConstant(3), StaticConstant{3}()),
                (DynamicVariable(:x), StaticVariable{:x}()),
                (
                    DynamicExpression((), :op, (; op = Returns(3))),
                    StaticExpression((), Returns(3)),
                    MoreStaticExpression{<:Any,Returns(3)}(()),
                ),
                (
                    DynamicExpression((DynamicVariable(:x), DynamicConstant(3)), :op, (; op = +)),
                    StaticExpression((DynamicVariable(:x), DynamicConstant(3)), +),
                    MoreStaticExpression{<:Any,+}((DynamicVariable(:x), DynamicConstant(3))),
                ),
            )
            hs = rand(UInt, 1000)
            for class ∈ classes
                for a ∈ class
                    for b ∈ class
                        @test a == b
                        @test isequal(a, b)
                        for h ∈ hs
                            @test hash(a, h)::UInt == hash(b, h)::UInt
                        end
                    end
                end
            end
        end
    end

    @testset "expression_is_constant" begin
        expressions_that_are_constant = (
            DynamicConstant(3), StaticConstant{3}(),
            DynamicExpression((StaticConstant{3}(), DynamicConstant(7)), :op, (; op = +)),
            StaticExpression((StaticConstant{3}(), DynamicConstant(7)), +),
            MoreStaticExpression{<:Any,+}((StaticConstant{3}(), DynamicConstant(7))),
        )
        expressions_that_are_not_constant = (
            DynamicVariable(:x), StaticVariable{:x}(),
            DynamicExpression((DynamicVariable(:x), DynamicConstant(3)), :op, (; op = +)),
            StaticExpression((DynamicVariable(:x), DynamicConstant(3)), +),
            MoreStaticExpression{<:Any,+}((DynamicVariable(:x), DynamicConstant(3))),
        )
        for e ∈ expressions_that_are_constant
            @test expression_is_constant(e)
        end
        for e ∈ expressions_that_are_not_constant
            @test !expression_is_constant(e)
        end
    end

    @testset "expression_constructor" begin
        f = x -> CallableExpressions.ExpressionConstructor.expression_constructor(typeof(x))
        @test f(DynamicExpression((), :op, (; op = Returns(7)))) == DynamicExpression
        @test f(StaticExpression((), Returns(7))) == StaticExpression
        @test f(MoreStaticExpression{<:Any,Returns(7)}(())) == MoreStaticExpression
    end

    @testset "expression_metadata" begin
        @test expression_metadata(DynamicExpression((), :op, (; op = Returns(7)))) == (; op = Returns(7))
        @test expression_metadata(StaticExpression((), Returns(7))) == nothing
        @test expression_metadata(MoreStaticExpression{<:Any,Returns(7)}(())) == nothing
    end

    @testset "expression_new" begin
        @test isone(length(methods(expression_new)))
        @test (
            expression_new(DynamicExpression, Returns(7), (), nothing) ==
            expression_new(DynamicExpression, Returns(7), (), (; op = Returns(7))) ==
            DynamicExpression((), :op, (; op = Returns(7))) ==
            expression_new(StaticExpression, Returns(7), (), nothing) ===
            StaticExpression((), Returns(7)) ==
            expression_new(MoreStaticExpression, Returns(7), (), nothing) ===
            MoreStaticExpression{<:Any,Returns(7)}(())
        )
    end

    @testset "expression_with_children" begin
        expressions = (
            DynamicExpression((DynamicVariable(:x), DynamicConstant(3)), :op, (; op = +)),
            StaticExpression((DynamicVariable(:x), DynamicConstant(3)), +),
            MoreStaticExpression{<:Any,+}((DynamicVariable(:x), DynamicConstant(3))),
        )
        for e ∈ expressions
            @test isempty(expression_children(expression_with_children(e, ())))
        end
    end

    @testset "expression_map_matched" begin
        @testset "substitution" begin
            expression = StaticExpression((DynamicVariable(:x), DynamicConstant(3)), +)
            is_match = isequal(StaticVariable{:x}())
            map = _ -> DynamicConstant(7)
            new_expression = StaticExpression((DynamicConstant(7), DynamicConstant(3)), +)
            @test expression_map_matched(is_match, map, expression) === new_expression
        end
        @testset "constant folding" begin
            expression = StaticExpression((DynamicConstant(7), DynamicConstant(3)), +)
            is_match = expression_is_constant
            map = e -> DynamicConstant(e((;)))
            new_expression = DynamicConstant(10)
            @test expression_map_matched(is_match, map, expression) === new_expression
        end
    end

    @testset "expression_into_type_domain" begin
        expression = DynamicExpression((DynamicVariable(:x), DynamicConstant(3)), :op, (; op = +)) 
        @test expression == expression_into_type_domain(expression)
        f = x -> Base.issingletontype(typeof(x))
        @test f(expression_into_type_domain(expression))
        @test f(expression_into_type_domain(DynamicVariable(:x)))
    end

    @testset "promotion" begin
        @test promote_type(StaticVariable{:x}, StaticVariable{:x}) == StaticVariable{:x}
        @test promote_type(StaticVariable{:x}, StaticVariable{:y}) == DynamicVariable
        @test promote_type(StaticVariable{:x}, DynamicVariable) == DynamicVariable
        @test promote_type(DynamicVariable, DynamicVariable) == DynamicVariable
        @test promote_type(StaticConstant{3}, StaticConstant{3}) == StaticConstant{3}
        @test promote_type(StaticConstant{3}, StaticConstant{7}) == DynamicConstant{Int}
        @test promote_type(StaticConstant{3}, DynamicConstant{Int}) == DynamicConstant{Int}
        @test promote_type(StaticConstant{3}, DynamicConstant{Float64}) == DynamicConstant{Float64}
        @test promote_type(DynamicConstant{Int}, DynamicConstant{Float64}) == DynamicConstant{Float64}
    end

    @testset "TermInterface.jl" begin
        using TermInterface: TermInterface
        @testset "Code quality (Aqua.jl)" begin
            Aqua.test_all(CallableExpressions)
        end
        c = (DynamicVariable(:x), DynamicConstant(3))
        e = StaticExpression(c, +)
        @test TermInterface.isexpr(e)
        @test TermInterface.iscall(e)
        @test TermInterface.arguments(e) === TermInterface.children(e) === c
        @test TermInterface.head(e) === TermInterface.operation(e) === +
        @test TermInterface.maketerm(StaticExpression, +, c, nothing, nothing) === e
        @test TermInterface.maketerm(StaticExpression{Tuple{},typeof(Returns(7))}, +, c, nothing, nothing) === e
    end

    @testset "AbstractTrees.jl" begin
        using AbstractTrees: AbstractTrees
        operations = (+, -)
        children = (
            (DynamicConstant(3), DynamicVariable(:x)),
            (DynamicVariable(:x), DynamicConstant(3)),
        )
        for op ∈ operations
            for chi ∈ children
                expressions = (
                    DynamicExpression(chi, :op, (; op = op)),
                    StaticExpression(chi, op),
                    MoreStaticExpression{<:Any,op}(chi),
                )
                for e ∈ expressions
                    @test AbstractTrees.nodevalue(e) === op
                    @test AbstractTrees.children(e) === chi
                end
            end
        end
    end

    @testset "ChainRulesCore.jl" begin
        using ChainRulesCore: ChainRulesCore
        using ChainRules: ChainRules
        using ChainRulesTestUtils: ChainRulesTestUtils
        @testset "Code quality (Aqua.jl)" begin
            Aqua.test_all(CallableExpressions)
        end
        @testset "CRC method tables test" begin
            ChainRulesTestUtils.test_method_tables()
        end
        # TODO: `test_frule` is buggy?
        rule_testers = (ChainRulesTestUtils.test_rrule,)
        @testset "rule tests" begin
            for test_rule ∈ rule_testers
                c0e = StaticExpression((), Returns(0.2))
                c0 = DynamicConstant(0.3)
                c1 = StaticExpression((c0,), sin)
                c2 = StaticExpression((c1,), cos)
                v0 = DynamicVariable(:x)
                v1 = StaticExpression((v0,), sin)
                v2 = StaticExpression((v1,), cos)
                unary_closure_op = let y = 0.3, unary_closure_operation
                    function unary_closure_operation(x)
                        x*y + 0.1
                    end
                end
                c3 = StaticExpression((c1,), unary_closure_op)
                v3 = StaticExpression((v1,), unary_closure_op)
                v4 = StaticExpression((v0, c0), *)
                constant_expressions = Any[c0, c1, c2, c3, c0e]
                variable_expressions = Any[v0, v1, v2, v3, v4]
                binary_operations = (((x, y) -> x * -y),)
                for bop ∈ binary_operations
                    ce = collect(constant_expressions)
                    for i ∈ eachindex(ce)
                        for j ∈ i:lastindex(ce)
                            l = ce[i]
                            r = ce[j]
                            push!(constant_expressions, StaticExpression((l, r), bop))
                        end
                    end
                    ve = collect(variable_expressions)
                    for i ∈ eachindex(ve)
                        for j ∈ i:lastindex(ve)
                            l = ve[i]
                            r = ve[j]
                            push!(variable_expressions, StaticExpression((l, r), bop))
                        end
                    end
                end
                a0 = (;)
                a1s = ((; x = c) for c ∈ -1.0:0.6:1.5)
                function t(e, a)
                    evaluated = CallableExpressions.ExpressionEvaluated.expression_evaluated
                    test_rule(evaluated, e, a, check_inferred = false)
                end
                for e ∈ constant_expressions
                    for a ∈ (a0, a1s...)
                        t(e, a)
                    end
                end
                for e ∈ variable_expressions
                    for a ∈ a1s
                        t(e, a)
                    end
                end
            end
        end
        @testset "rule tests, matrix values" begin
            for test_rule ∈ rule_testers
                c0e = StaticExpression((), Returns([0.1 0.2; 0.3 0.4]))
                c0 = DynamicConstant([0.1 0.4; 0.3 0.2])
                c1 = StaticExpression((c0,), sin)
                c2 = StaticExpression((c1,), cos)
                v0 = DynamicVariable(:x)
                v1 = StaticExpression((v0,), sin)
                v2 = StaticExpression((v1,), cos)
                unary_closure_op = let y = [0.4 0.3; 0.2 0.1], unary_closure_operation
                    function unary_closure_operation(x)
                        x .* y .+ [0.3 0.4; 0.2 0.1]
                    end
                end
                c3 = StaticExpression((c1,), unary_closure_op)
                v3 = StaticExpression((v1,), unary_closure_op)
                v4 = StaticExpression((v0, c0), *)
                constant_expressions = Any[c0, c1, c2, c3, c0e]
                variable_expressions = Any[v0, v1, v2, v3, v4]
                a0 = (;)
                a1s = ((; x = c) for c ∈ ([0.1 0.2; 0.3 0.4], [0.1 0.4; 0.3 0.2], [0.3 0.4; 0.2 0.1]))
                function t(e, a)
                    evaluated = CallableExpressions.ExpressionEvaluated.expression_evaluated
                    test_rule(evaluated, e, a, check_inferred = false)
                end
                for e ∈ constant_expressions
                    for a ∈ (a0, a1s...)
                        t(e, a)
                    end
                end
                for e ∈ variable_expressions
                    for a ∈ a1s
                        t(e, a)
                    end
                end
            end
        end
    end

    @testset "DifferentiationInterface.jl" begin
        using Diffractor: Diffractor
        using Zygote: Zygote
        using DifferentiationInterface: DifferentiationInterface
        backends = (DifferentiationInterface.AutoDiffractor(), DifferentiationInterface.AutoZygote())
        operators = (DifferentiationInterface.value_and_derivative, DifferentiationInterface.value_and_gradient)
        for operator ∈ operators
            f = differentiation_interface_backend_mapping_maker(operator, 0.1)
            @test allequal(map(f, backends))
        end
    end
end
